package com.example.test1.ViewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.test1.Model.Repository
import com.example.test1.Utils.Contact
import java.util.ArrayList

class ContactsViewModel @ViewModelInject constructor(
        private val repository: Repository) : ViewModel() {
    val contactsData: MutableLiveData<ArrayList<Contact>>
        get() = repository.fetchContacts()

}