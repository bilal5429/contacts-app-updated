package com.example.test1.Modules

import android.app.Application
import com.example.test1.Adapter.MyRecyclerViewAdapter
import com.example.test1.Model.ContactModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
class UtilsModule {

//    @Provides
//    fun provideString(): String {
//        return "This is a test message by Bilal Khan."
//    }

    @Provides
    fun provideContactModel(context: Application?): ContactModel {
        return ContactModel(context!!)
    }

    @Provides
    fun contactsAdapter(): MyRecyclerViewAdapter {
        return MyRecyclerViewAdapter()
    }
}