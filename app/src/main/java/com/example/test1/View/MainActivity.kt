 package com.example.test1.View

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.test1.Adapter.MyRecyclerViewAdapter
import com.example.test1.Adapter.MyRecyclerViewAdapter.ItemClickListener
import com.example.test1.R
import com.example.test1.Utils.Contact
import com.example.test1.ViewModel.ContactsViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), ItemClickListener {
    private var contact: Contact? = null
    private var viewModel: ContactsViewModel? = null

     var contactsAdapter: MyRecyclerViewAdapter? = null
         @Inject set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProvider(this).get(ContactsViewModel::class.java)
        contact = Contact()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS), PERMISSIONS_REQUEST_READ_CONTACTS)
        } else {
            displayContacts()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                displayContacts()
            } else {
                Toast.makeText(this, "Until you grant the permission, we cannot display the names", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onItemClick(view: View?, position: Int) {}

    fun displayContacts() {

        viewModel!!.contactsData.observe(this, (Observer { l: ArrayList<Contact> ->
            for (o in l) {
                contact = o
            }

            val recyclerView: RecyclerView?
            recyclerView = findViewById(R.id.rvContacts)
            recyclerView.setLayoutManager(LinearLayoutManager(this))
            contactsAdapter!!.setContactsList(l)
            recyclerView.setAdapter(contactsAdapter)
        } as Observer<ArrayList<Contact>>))
    }

    companion object {
        private const val PERMISSIONS_REQUEST_READ_CONTACTS = 100
    }
}