package com.example.test1.Model

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.provider.ContactsContract
import androidx.lifecycle.MutableLiveData
import com.example.test1.Utils.Contact
import java.util.*

class ContactModel(val context: Context) {
    var cursor: Cursor? = null
    private val responseLiveData = MutableLiveData<ArrayList<Contact>>()
    @SuppressLint("Recycle")
    fun fetchContacts(): MutableLiveData<ArrayList<Contact>> {
        val contactsList = ArrayList<Contact>()
        try {
            cursor = context.contentResolver
                    .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null)
            if ( cursor == null) {
                error("Assertion failed")
            }
            val Idx = cursor!!.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID)
            val nameIdx = cursor!!.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
            val phoneNumberIdx = cursor!!.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
            cursor!!.moveToFirst()
            val ids: MutableSet<String> = HashSet()
            do {
                val contactId = cursor!!.getString(Idx)
                if (!ids.contains(contactId)) {
                    ids.add(contactId)
                    val name = cursor!!.getString(nameIdx)
                    val phoneNumber = cursor!!.getString(phoneNumberIdx)
                    val contact1 = Contact()
                    contact1.contactName = name
                    contact1.phoneNo = phoneNumber
                    contactsList.add(contact1)
                }
            } while (cursor!!.moveToNext())
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (cursor != null) {
                cursor!!.close()
            }
        }
        responseLiveData.value = contactsList
        return responseLiveData
    }

}