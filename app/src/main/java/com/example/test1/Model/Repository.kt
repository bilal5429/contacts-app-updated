package com.example.test1.Model

import android.app.Application
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.test1.Utils.Contact
import java.util.ArrayList
import javax.inject.Inject

class Repository @Inject constructor(context: Application) {
    val context: Context

    @JvmField
    @Inject
    var contactModel: ContactModel? = null
    fun fetchContacts(): MutableLiveData<ArrayList<Contact>> {
        return contactModel!!.fetchContacts()
    }

    init {
        this.context = context
    }
}